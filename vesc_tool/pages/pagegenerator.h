/*
    Copyright 2018 Benjamin Vedder	benjamin@vedder.se

    This file is part of VESC Tool.

    VESC Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VESC Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */

#ifndef PAGEGENERATOR_H
#define PAGEGENERATOR_H

#include <QWidget>
#include <QTimer>
#include <QElapsedTimer>
#include "vescinterface.h"
#include <QWidget>
#include "vescinterface.h"
#include "widgets/paramtable.h"
#ifdef HAS_SERIALPORT
#include <QSerialPortInfo>
#endif

namespace Ui {
class PageGenerator;
}

class PageGenerator : public QWidget
{
    Q_OBJECT

public:
    explicit PageGenerator(QWidget *parent = 0);
    ~PageGenerator();

    VescInterface *vesc() const;
    void setVesc(VescInterface *vesc);

private:
    Ui::PageGenerator *ui;
    VescInterface *mVesc;

};

#endif // PAGEGENERATOR_H
