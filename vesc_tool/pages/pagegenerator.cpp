/*
    Copyright 2018 Benjamin Vedder	benjamin@vedder.se

    This file is part of VESC Tool.

    VESC Tool is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VESC Tool is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    */

#include "pagegenerator.h"
#include "ui_pagegenerator.h"

#include <QDebug>
#include <QFileDialog>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <cmath>

#ifdef HAS_SERIALPORT
#include <QSerialPortInfo>
#endif

PageGenerator::PageGenerator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PageGenerator)
{
    ui->setupUi(this);
    layout()->setContentsMargins(0, 0, 0, 0);
    mVesc = 0;
}

PageGenerator::~PageGenerator()
{
    delete ui;
}
VescInterface *PageGenerator::vesc() const
{
    return mVesc;
}
void PageGenerator::setVesc(VescInterface *vesc)
{
    mVesc = vesc;

    ui->generatorTab->addParamRow(mVesc->appConfig(), "gen.op_mode");
    ui->generatorTab->addParamRow(mVesc->appConfig(), "gen.dummy_mode_start_speed");
    ui->generatorTab->addParamRow(mVesc->appConfig(), "gen.dummy_mode_stop_speed");
    ui->windturbineTab->addParamRow(mVesc->appConfig(), "gen.load_mode_wind_speed");
    ui->windturbineTab->addParamRow(mVesc->appConfig(), "gen.load_mode_turbine_coeff");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_max_curr");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_step_curr");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_stop_pwr_coeff");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_min_pwr_active");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_start_speed");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_scan_tout");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_gen_tout");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_back_tout");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_ful_bat_v");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_ful_der_v");
    ui->mpptTab->addParamRow(mVesc->appConfig(), "gen.mppt_en_dc_mfet");





}




